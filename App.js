import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import { Card, Block } from "./components";
import Navigation from './navigation';
import { ScrollView } from 'react-native-gesture-handler';



export default function App() {
  
  return (
     <Navigation></Navigation>


  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
