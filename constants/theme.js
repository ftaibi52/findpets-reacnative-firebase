const colors = {
    accent: "#2BDA8F",
    primary: "#0BC5B9",
    secondary: "#36B818",
    tertiary: "#C2B619",
    black: "#323643",
    white: "#FFFFFF",
    gray: "#9DA3B4",
    gray2: "#C5CCD6",
    gray3: "#F0F0F0",
    gray4: "#C5CCD6",
    grayLight: "#F7F7F7",
    greenIoon: "#9BAB1F",
    walk: "#4E72FF",
    fire: "#FF5629",
    distance: "#45EB42",
  };
  
  
  const sizes = {
    // global sizes
    base: 16,
    font: 16,
    radius: 20,
    padding: 20,
    marginHorizontal: 5,
    paddingTop: 30,
    paddingVertical: 25,
    paddingHorizontal: 40,    
    backgroundColor: "#761C27",
    buttonWidth: 60,
  
    // font sizes
    bigTitle: 38,
    h1: 39,
    h2: 29,
    h3: 19,
    title: 18,
    header: 24,
    body: 14,
    caption: 12,
    small: 8,
  };
  
  const fonts = {
    coloredTitle: {
      fontSize: 80,
      fontFamily: "Rubik-Light",
      fontWeight: 'bold',
      color: colors.accent,
      paddingTop: 50, 
    },
    h1: {
      fontFamily: "Rubik-Light",
      fontSize: sizes.h1
    },
    h2: {
      fontFamily: "Rubik-Medium",
      fontSize: sizes.h2
    },
    h3: {
      fontFamily: "Rubik-Regular",
      fontSize: sizes.h3
    },
    header: {
      fontFamily: "Rubik-Bold",
      fontSize: sizes.header
    },
    title: {
      fontFamily: "Rubik-Regular",
      fontSize: sizes.title
    },
    cardsText: {
      fontFamily: "Rubik-Light",
      fontSize: sizes.bigTitle,
      color: colors.gray,
    },
    cardsLabels: {
      fontFamily: "Rubik-Light",
      fontSize: sizes.bigTitle,
      
    },
    body: {
      fontSize: sizes.body
    },
    caption: {
      fontSize: sizes.caption
    },
    small: {
      fontSize: sizes.small
    }
  };
  
  export { colors, sizes, fonts };