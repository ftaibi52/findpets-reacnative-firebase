import { AsyncStorage, Alert } from 'react-native';


const storeData = async (key, value, message) => {

    if(message == null){
        message = "Se ha producido un error al guardar los credenciales";
    }

    try {   
        await AsyncStorage.setItem(key, value);
    } catch (error) {
        showAlert("Error al guardar los datos", message + ": " + error);
        return error;
    }
};

const retrieveData = async (key) => {

    try {
      const data = await AsyncStorage.getItem(key);
      if (data !== null) {
        return data;
      }
    } catch (error) {
        showAlert("Error al cargar los credenciales", "Se ha producido un error al cargar los credenciales: " + error);
        return error;
    }
};


export {storeData, retrieveData}
