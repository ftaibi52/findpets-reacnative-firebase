export default class User {

    constructor(name, lastname, birthday, genre, complexion, ictus, description){
        this.name = name;
        this.lastname = lastname;
        this.birthday = birthday;
        this.genre = genre;
        this.complexion = complexion;
        this.ictus = ictus;
        this.description = description;
    }

}