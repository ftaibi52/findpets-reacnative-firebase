import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList, Dimensions, Alert, TouchableHighlight, TextInput } from 'react-native';
import { Card, Block, Button } from "../components";
import { Component } from "react";
import { ScrollView } from 'react-native-gesture-handler';
import { db } from "../src/config";
import { Icon } from 'react-native-elements';
import { auth } from 'firebase';

let addItem = item => {
    db.ref('/items').push({
        name: item
    });
};


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            likes_d: ' ',
            modalVisible: false,
            userSelected: [],
            dogs: [],
            like: 0,
            likesss: 0
        };
    }

    static navigationOptions = {
        title: 'Home',
    };

    async componentDidMount() {
        let dogs = [];
        let i = 1;
        await db.ref().on("value", (snapshot) => {
            snapshot.val().forEach(function (element) {
                dogs.push({
                    id: i,
                    name: element.Name,
                    desctription: element.Description,
                    like: element.Like,
                    photo: element.Photo,
                    sex: element.Sex
                });
                i++;
            });
            i = 1;
            this.setState({ dogs: dogs });

        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
        });

        await db.ref().on("value", (snapshot) => {

            snapshot.val().forEach((element) => {
                this.setState({ like: element.Like });
            });


        });

    }

    async like(id, likes) {

        let likes_t = likes + 1;

        await db.ref(id).update({

            Like: likes_t

        });
        console.log(likes_t);
    }

    render() {

        const { navigation } = this.props;
        console.log(this.state.like);
        return (

            <View style={styles.container}>
                <ScrollView>
                    <FlatList
                        style={styles.contentList}
                        columnWrapperStyle={styles.listContainer}
                        data={this.state.dogs}
                        keyExtractor={(item) => {
                            return item.id;
                        }}
                        renderItem={({ item }) => {
                            return (
                                //   <TouchableOpacity style={styles.card} onPress={() => { this.clickEventListener(item) }}>
                                <TouchableOpacity style={styles.card} onPress={
                                    () => navigation.navigate('Profile', {
                                        id: item.id,
                                        name: item.name,
                                        des: item.desctription,
                                        photo: item.photo,
                                        sex: item.sex
                                    })}  >

                                    <Image style={styles.image} source={{ uri: item.photo }} />
                                    <View >
                                        <Text style={styles.name}>{item.name}</Text>

                                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', width: 50, marginTop: 40 }}>
                                            <Icon raised name="favorite" color="red" onPress={
                                                async () => {
                                                    await db.ref(item.id).once('value', (data) => {

                                                        this.setState({ likesss: data.val().Like })

                                                    })

                                                    await db.ref(item.id).update({

                                                        Like: this.state.likesss + 1

                                                    })
                                                }
                                            } />

                                            <View>
                                                <Text style={styles.count}>{item.like} </Text>
                                            </View>
                                        </View>

                                    </View>
                                    <View style={{ marginLeft: 100 }}>
                                        <Text style={{ backgroundColor: '#F45A5A', borderRadius: 5, textAlign: "justify", color: "white", padding: 3 }}>{item.sex}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        }} />


                </ScrollView>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',

    },
    contentList: {
        flex: 1,
    },
    cardContent: {
        marginLeft: 20,
        marginTop: 10
    },
    image: {
        width: 90,
        height: 90,
        borderRadius: 30,
        borderWidth: 2,
        borderColor: "#ebf0f7",
        marginRight: 5
    },

    card: {
        shadowColor: '#00000021',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 12,

        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        backgroundColor: "white",
        padding: 10,
        flexDirection: 'row',
        borderRadius: 30,
    },

    name: {
        fontSize: 18,
        flex: 1,
        alignSelf: 'center',
        color: "#3399ff",
        fontWeight: 'bold'
    },
    count: {
        fontSize: 14,
        flex: 1,
        alignSelf: 'center',
        color: "#6666ff"
    },
    followButton: {
        marginTop: 10,
        height: 35,
        width: 100,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        backgroundColor: "white",
        borderWidth: 1,
        borderColor: "#dcdcdc",
    },
    followButtonText: {
        color: "#dcdcdc",
        fontSize: 12,
    },
});  
