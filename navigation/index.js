import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';


import Home from "../views/home";
import Profile from "../views/profile";


const screens = createStackNavigator({

  Home,
  Profile,

  });

export default createAppContainer(screens);