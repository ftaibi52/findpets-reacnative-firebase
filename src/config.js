import Firebase from 'firebase';

let config = {
    apiKey: "AIzaSyD7MOKoruXGQjmbQaF4jNmx1vjSEpDqRA0",
    authDomain: "boomva-18eb7.firebaseapp.com",
    databaseURL: "https://boomva-18eb7.firebaseio.com",
    projectId: "boomva-18eb7",
    storageBucket: "boomva-18eb7.appspot.com",
    messagingSenderId: "685356143809",
    appId: "1:685356143809:web:80f5d6eaf3dfb134d7b040"
};
let app = Firebase.initializeApp(config);

export const db = app.database();